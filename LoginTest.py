from selenium import webdriver
import unittest
import time


class loginMemriseTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Firefox(
            executable_path=r'/home/rafal/RAFAŁ/ITtesty/x_python_jakTestowacPL/drivers for selenium/Firefox/geckodriver')
        self.base_url = 'https://www.memrise.com/?ref=discuvver'
        self.driver.maximize_window()
        self.login = 'maxspider99'
        self.password = 'Wrzesien!23'

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_login_to_user_account(self):
        expected_user_profil_name = 'maxspider99'

        self.driver.get(self.base_url)
        time.sleep(1)
        login_button = self.driver.find_element_by_xpath('//span[text()="Log in"]').click()
        time.sleep(1)
        cookies_bar = self.driver.find_element_by_xpath('//a[@aria-label="allow cookies"]').click()
        time.sleep(1)
        user_name = self.driver.find_element_by_xpath('//input[@name="username"]')
        user_name.send_keys(self.login)
        user_pass = self.driver.find_element_by_xpath('//input[@name="password"]')
        user_pass.send_keys(self.password)
        submit_button = self.driver.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(1)
        popup = self.driver.switch_to.window(self.driver.window_handles[0])
        time.sleep(1)
        popup_close = self.driver.find_element_by_xpath('//*[@id="promotion-modal"]//button[@type="button"]').click()
        user_profil = self.driver.find_element_by_xpath('//h3[@class="name"]').text
        self.assertEqual(user_profil, expected_user_profil_name, f'Expected user profil name is differ from {user_profil}')
        time.sleep(1)
        menu_button = self.driver.find_element_by_xpath('//span[@class="info-card-picture"]').click()
        logout_button = self.driver.find_element_by_xpath('//span[text()="Log out"]').click()
        time.sleep(1)
        logo_button = self.driver.find_element_by_xpath('//a[@title="Home"]').click()
        time.sleep(1)

    def test_login_via_google_account(self):
        expected_site_title = 'Logowanie – Konta Google'

        self.driver.get(self.base_url)
        time.sleep(1)
        login_button = self.driver.find_element_by_xpath('//span[text()="Log in"]').click()
        time.sleep(1)
        #cookies_bar = self.driver.find_element_by_xpath('//a[@aria-label="allow cookies"]').click()
        #time.sleep(1)
        google_login_button = self.driver.find_elements_by_xpath('//form[@id="login"]/a')
        target_element = google_login_button[0].click()
        time.sleep(2)
        google_login_site = self.driver.title
        self.assertEqual(google_login_site, expected_site_title, f'Expected site title differ from actual for page url')

    def test_login_via_facebook_account(self):
        expected_site_title = 'Log into Facebook | Facebook'

        self.driver.get(self.base_url)
        time.sleep(1)
        login_button = self.driver.find_element_by_xpath('//span[text()="Log in"]').click()
        time.sleep(1)
        #cookies_bar = self.driver.find_element_by_xpath('//a[@aria-label="allow cookies"]').click()
        #time.sleep(1)
        facebook_login_button = self.driver.find_elements_by_xpath('//form[@id="login"]/a')
        target_element = facebook_login_button[1].click()
        time.sleep(2)
        facebook_login_site = self.driver.title
        self.assertEqual(facebook_login_site, expected_site_title, f'Expected site title differ from actual for page url')
