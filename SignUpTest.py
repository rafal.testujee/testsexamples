from selenium import webdriver
import unittest
import time

class openingAccountXtimes(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Firefox(
            executable_path=r'/home/rafal/RAFAŁ/ITtesty/x_python_jakTestowacPL/drivers for selenium/Firefox/geckodriver')
        self.base_url = 'https://www.memrise.com/?ref=discuvver'
        self.driver.maximize_window()

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_open_account(self):
        user_name = 'kolisos12'
        password = 'kolorowerkrdki'

        for i in range(2):
            self.driver.get(self.base_url)
            signup_button = self.driver.find_element_by_xpath(
                '//li[@class="header-nav-item header-nav-signup colored "]/a').click()
            time.sleep(1)
        #cookies_bar = self.driver.find_element_by_xpath('//a[@aria-label="allow cookies"]').click()
        #time.sleep(1)
            choose_course_button = self.driver.find_element_by_xpath('//h2[text()="Spanish (Spain)"]').click()
            time.sleep(2)
            start_course_button = self.driver.find_element_by_xpath('//button[text()="Start from the beginning"]').click()
            time.sleep(2)
            email_adress = 'money' + str(i) + '@wer.pl'
            user_name_input = self.driver.find_element_by_xpath('//input[@id="username"]')
            user_name_input.send_keys(user_name)
            user_email_input = self.driver.find_element_by_xpath('//input[@id="emailAddress"]')
            user_email_input.send_keys(email_adress)
            password_input = self.driver.find_element_by_xpath('//input[@id="password"]')
            password_input.send_keys(password)
