from selenium import webdriver
import unittest
import time


class MemriseSmokeTests(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Firefox(
            executable_path=r'/home/rafal/RAFAŁ/ITtesty/x_python_jakTestowacPL/drivers for selenium/Firefox/geckodriver')
        self.base_url = 'https://www.memrise.com/?ref=discuvver'
        self.driver.maximize_window()

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_main_site_title(self):
        driver = self.driver
        driver.get(self.base_url)
        title = driver.title
        print(f'Actual title: {title}')
        self.assertEqual(title, 'Memrise - Learn a language. Meet the world.',
                         'Expected page title differ from actual for page url: https://www.memrise.com/?ref=discuvver')

    def test_main_site_element_check(self):
        driver = self.driver
        driver.get(self.base_url)
        time.sleep(1)
        element_text = driver.find_element_by_xpath('//h1[@class="masthead-heading--h1"]').text
        print(f'Actual element text: {element_text}')
        self.assertEqual(element_text, 'The fastest way to learn a language.',
                         'Expected element_text differ from actual for page url: https://www.memrise.com/?ref=discuvver')

    def test_courses_site_pass(self):
        driver = self.driver
        driver.get(self.base_url)
        time.sleep(1)
        courses_button = self.driver.find_element_by_xpath('//span[text()="Courses"]').click()
        time.sleep(1)
        site_text = driver.find_element_by_xpath('//h1[@class="page-breadcrumb"]').text
        print(f'Actual site text: {site_text}')
        self.assertEqual(site_text, 'Courses',
                    'Expected site_text differ from actual for page url: https://www.memrise.com/pl/courses/polish/#')
